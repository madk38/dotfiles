alias vim='nvim'
alias py='python'
alias ls='exa'
alias la='exa -la'
alias rem='trash-put'
alias code='codium'
alias kcl='kdeconnect-cli -n COR-AL00'
alias bon='bluetoothctl power on && bluetoothctl connect 00:16:94:3D:5F:69'
alias boff='bluetoothctl power off'
alias dnd='dragon-drop'

function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end

abbr -a dotdot --regex '^\.\.+$' --function multicd

function last_history_item; echo $history[1]; end
abbr -a !! --position anywhere --function last_history_item

abbr -a cl clear
abbr -a chx 'chmod +x'
abbr -a sys 'sudo systemctl'
abbr -a ufw 'sudo ufw'
abbr -a df 'df -h'
abbr -a du 'du -h'
abbr -a lg lazygit
# abbr -a - 'cd -'

# js
abbr -a ns 'npm start'
abbr -a nr 'npm run'
abbr -a rn 'npx react-native'
abbr -a ys 'yarn start'
abbr -a yb 'yarn build'
abbr -a ysd 'yarn start:dev'

# git
abbr -a ga 'git add'
abbr -a gaa 'git add --all'
abbr -a gb 'git branch'
abbr -a gc 'git commit'
abbr -a gcm 'git commit -m'
abbr -a gcam 'git commit -a -m'
abbr -a gcl 'git clone'
abbr -a gd 'git diff'
abbr -a gl 'git pull'
abbr -a glm 'git pull origin master'
# abbr -a glo 'git log'
abbr -a gp 'git push'
abbr -a gpf 'git push --force-with-lease'
abbr -a grc 'git rebase --continue'
# abbr -a gsh 'git stash'
# abbr -a gshd 'git stash drop'
# abbr -a gshp 'git stash pop'
# abbr -a gshl 'git stash list'
# abbr -a gst 'git status'
abbr -a gs 'git switch'
abbr -a gsc 'git switch -c'
abbr -a gsm 'git switch master'
abbr -a gwa 'git worktree add'
abbr -a gwl 'git worktree list'
abbr -a gwr 'git worktree remove'

# interactive operations
abbr -a rmf 'rm -rf'

# activate anaconda
abbr -a base 'conda activate base'

# services
abbr -a mongost 'sudo systemctl start mongodb.service'
abbr -a mongokl 'sudo systemctl stop mongodb.service'
abbr -a mysqlst 'sudo systemctl start mysql.service'
abbr -a mysqlkl 'sudo systemctl stop mysql.service'
abbr -a dockerst 'sudo systemctl start docker'

# shortcuts
abbr -a dl 'cd ~/Downloads'
abbr -a ytd youtube-dl

# Paru
abbr -a prupg 'paru -Syu'
abbr -a prin 'paru -S'
abbr -a prre 'paru -Rs'

# Pacman - https://wiki.archlinux.org/index.php/Pacman_Tips
abbr -a pacupg 'sudo pacman -Syu'
abbr -a pacin 'sudo pacman -S'
abbr -a pacre 'sudo pacman -Rs'

# apt
abbr -a aptupg 'sudo apt update && sudo apt upgrade -y'
abbr -a aptin 'sudo apt install'
abbr -a aptre 'sudo apt uninstall'

# k8s
abbr -a k kubectl

# vim:et ts=4 sw=4
