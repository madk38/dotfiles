function __car_compile -a file_in file_out flags
    if test -e $file_out; and test (stat -c %Y $file_out) -gt (stat -c %Y $file_in)
        return 0
    end
    echo Compiling...
    set -l CC g++
    if test (file $file_in | grep 'C source')
        set CC gcc
    end
    set -l CFLAGS -Wall -Wextra -DDEBUG $flags
    $CC $CFLAGS $file_in -o $file_out
end

# vim:et ts=4 sw=4
