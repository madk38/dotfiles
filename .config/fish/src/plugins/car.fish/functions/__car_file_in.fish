function __car_file_in -a file_in
    if test -z $file_in
        echo sol.cpp
    else
        echo $file_in
    end
end

# vim:et ts=4 sw=4
