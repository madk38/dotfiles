function __car_file_out -a file_in
    set -l file_out (string split -r -m2 -f2,3 / (pwd) | string join '_')
    set file_out (string join '_' $file_out (basename $file_in .cpp))
    set file_out (string join '/' '/tmp' $file_out)
    echo $file_out
end

# vim:et ts=4 sw=4
