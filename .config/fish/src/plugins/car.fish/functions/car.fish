function car -d "Compile and run"
    set_color green
    set -l file_in (__car_file_in $argv[1])
    set -l file_out (__car_file_out $file_in)
    set -e argv[1]
    __car_compile $file_in $file_out $argv
    or return 1
    echo Running...
    set_color normal
    $file_out $argv[2..]
end

# vim:et ts=4 sw=4
