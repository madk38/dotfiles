function crt -d "Compile, run and test"
    set_color green
    set -l file_in (__car_file_in $argv[1])
    set -l file_out (__car_file_out $file_in)
    __car_compile $file_in $file_out
    or return 1
    echo Testing...
    for file in in*
        set -l index (string sub -s 3 -l 1 $file)
        set -l in (string join '' in $index .txt)
        set -l out (string join '' out $index .txt)
        # $file_out < $in | diff -y --color=always $out
        delta -s --dark ($file_out < $in | psub) $out
        and echo Test $index passed
        or begin
            set_color red
            echo Test $index failed
        end
    end
end

# vim:et ts=4 sw=4
