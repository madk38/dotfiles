
function yg
    git remote set-url origin (git remote get-url origin | sed -e 's#https://#git@#' -e 's#/#:#')
end

# vim:et ts=4 sw=4
