function __get_quality -a quality
    switch $quality
        case high h
            echo prepress
        case medium m
            echo ebook
        case low l
            echo screen
        case '*'
            echo default
    end
end

function compress -d "Compress pdf"
    set -l options (fish_opt --short=q --long=quality --optional-val)
    argparse -n compress -N 1 $options -- $argv
    or return
    set -l quality (__get_quality $_flag_quality)
    for i in $argv
        set -l out_file "compressed_$i"
        echo out file is $out_file
        gs -sDEVICE=pdfwrite \
            -dCompatibilityLevel=1.4 \
            -dPDFSETTINGS=/$quality \
            -dNOPAUSE \
            -dQUIET \
            -dBATCH \
            -sOutputFile=$out_file \
            $i
    end
end

# vim:et ts=4 sw=4
