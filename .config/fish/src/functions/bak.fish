### backup file
# usage: bak <file/s>
function bak -d "create backup of given file/folder"
    for file in $argv
        string match -q -r '^(?<basename>.+).bak$' $file
        and mv -i $file $basename
        or mv -i $file $file.bak
    end
end

# vim:et ts=4 sw=4
