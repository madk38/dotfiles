function pdfmerge -d "Combine PDF documents into one file"
    set -l options (fish_opt --short=o --long=output --required-val)
    argparse -n pdfmerge -N 1 $options -- $argv
    or return
    if not set -q _flag_output
        echo "bef [-o|--output] [ARGUMENTS...]"
        return 0
    end

    gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=$_flag_output -dBATCH $argv
end

# vim:et ts=4 sw=4
