function topdf -d "Subtract given hrs from timestamp"
    if not count $argv > /dev/null
        echo "topdf [ARGUMENTS...]"
        return 0
    end
    libreoffice --convert-to pdf $argv
end

# vim:et ts=4 sw=4
