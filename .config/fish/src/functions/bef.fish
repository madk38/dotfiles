function bef -d "Subtract given hrs from timestamp"
    set -l options (fish_opt --short=t --long=time --required-val)
    argparse -n bef -N 1 $options -- $argv
    or return
    if not set -q _flag_time
        echo "bef [-t|--time] [ARGUMENTS...]"
        return 0
    end
    set -l subtract (math $_flag_time \* 3600)
    for i in $argv
        touch -d (date -d @(math (stat --printf='%Y' $i) - $subtract) +%c) $i
    end
end

# vim:et ts=4 sw=4
