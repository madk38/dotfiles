### create directories if not exists and cd
# usage: mkd <file>
function mkd -d "create directory and change to created directory"
    mkdir -p $argv[1] && cd $argv[1]
end

# vim:et ts=4 sw=4
