if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert

    # Then execute the vi-bindings so they take precedence when there's a conflict.
    # Without --no-erase fish_vi_key_bindings will default to
    # resetting all bindings.
    # The argument specifies the initial mode (insert, "default" or visual).
    fish_vi_key_bindings --no-erase insert
end

function fish_title
    set -q argv[1]; or set argv fish
    # Looks like ~/d/fish: git log
    # or /e/apt: fish
    echo $argv (fish_prompt_pwd_dir_length=1 prompt_pwd)
end

set -U fish_greeting

set -xg LANG en_US.UTF-8
set -xg EDITOR nvim
set -xg VISUAL nvim
set -xg BROWSER /usr/bin/firefox-developer-edition
set -xg PRETTIERD_LOCAL_PRETTIER_ONLY true

## Go
set -xg GOPATH $HOME/.go
fish_add_path --append --path $GOPATH/bin

## Local binaries
fish_add_path --append --path $HOME/.local/bin

## Android Studio
set -xg ANDROID_SDK_ROOT $HOME/Android/Sdk
fish_add_path --append --path $ANDROID_SDK_ROOT/emulator
fish_add_path --append --path $ANDROID_SDK_ROOT/platform-tools

# colored man pages
set -xU LESS_TERMCAP_md (printf "\e[01;31m")
set -xU LESS_TERMCAP_me (printf "\e[0m")
set -xU LESS_TERMCAP_so (printf "\e[01;44;33m")
set -xU LESS_TERMCAP_se (printf "\e[0m")
set -xU LESS_TERMCAP_us (printf "\e[01;32m")
set -xU LESS_TERMCAP_ue (printf "\e[0m")

# colors
set fish_color_normal red
set fish_color_command green
set fish_color_keyword yellow
set fish_color_quote yellow
set fish_color_redirection white
set fish_color_end white
set fish_color_error red
set fish_color_param white
set fish_color_comment brblack
set fish_color_selection normal
set fish_color_operator normal
set fish_color_escape normal
set fish_color_autosuggestion normal
set fish_color_cancel normal
set fish_color_search_match normal

# Pager color
set fish_pager_color_progress green
set fish_pager_color_background --background=none
set fish_pager_color_prefix cyan
set fish_pager_color_completion blue
set fish_pager_color_description blue
# set fish_pager_color_selected_background
# set fish_pager_color_selected_prefix
# set fish_pager_color_selected_completion
# set fish_pager_color_selected_description
# set fish_pager_color_secondary_background
# set fish_pager_color_secondary_prefix
# set fish_pager_color_secondary_completion
# set fish_pager_color_secondary_description

set -xg FISH_HOME $HOME/.config/fish/src
source $FISH_HOME/aliases.fish
set fish_function_path $fish_function_path $FISH_HOME/functions

# starship init fish | source
# source /opt/asdf-vm/asdf.fish
# source ~/.asdf/asdf.fish
# source ~/.asdf/plugins/java/set-java-home.fish
mise activate fish | source
zoxide init fish | source
direnv hook fish | source

set -x FZF_DEFAULT_OPTS '--cycle --layout=reverse --height=40%'
set fzf_preview_dir_cmd exa --all --color=always
fzf_configure_bindings --git_status --history=\cr --variables --directory=\ct --git_log

# vim:et ts=4 sw=4
