#!/bin/bash

theme="style_7"
dir="$HOME/.config/rofi/launchers"

rofi -no-lazy-grab -show drun -modi drun -theme "$dir/$theme"
