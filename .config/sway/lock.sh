#!/bin/bash

swaylock --daemonize \
	--screenshot \
	--effect-blur 10x10 \
	--effect-vignette 0.5:0.5 \
	--fade-in 0.2 \
	--clock \
	"$@"
