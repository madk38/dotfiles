#!/bin/bash

# uniform look of qt apps under other environments e.g. i3
# There are two possible solutions
# 1. fake the running desktop environments
# export XDG_CURRENT_DESKTOP=KDE
# 2. using qt5ct
if [[ "$XDG_CURRENT_DESKTOP" != "KDE" ]]; then
	export QT_QPA_PLATFORMTHEME=qt5ct
fi
