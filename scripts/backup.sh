#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

DEST="/run/media/kanhaiya/22C68AF3C68AC70F/Backup"

rm -rf $DEST/.config
cp -r "$HOME"/.config $DEST

rm -rf $DEST/.mozilla
cp -r "$HOME"/.mozilla/ $DEST

rsync --archive --progress --verbose --recursive \
	--filter=":- .gitignore" --exclude node_modules --exclude '.yarn' \
	Kanhaiya Documents Downloads Pictures Videos \
	/run/media/kanhaiya/22C68AF3C68AC70F/Backup
